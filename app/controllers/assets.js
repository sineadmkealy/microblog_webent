/**
 * align logic, making decisions etc
 * js files: the database queries are using www.mongoosejs.com
 */

'use strict';

// object literal
// name:value pair
// refer to HAPI documentation
exports.servePublicDirectory = {
  directory: {
    path: 'public',
  },

};