/**
 * Accounts controller to handle user accounts: signup, login, authenticate etc
 * js files: the database queries are using www.mongoosejs.com
 */

'use strict';

const User = require('../models/user'); // import the Model
const Administrator = require('../models/administrator'); // import the Model
const Joi = require('joi'); // validation/error reporting partial in the handlers

exports.main = {
  auth: false, //TO disable the protected strategy for this route
  handler: function (request, reply) {
    reply.view('main', { title: 'Welcome to Microblog' });
  },
};
//--------------------------------------------------------------------------------------------------------------
exports.signup = {
  auth: false,
  handler: function (request, reply) {
    reply.view('signup', { title: 'Sign up for Tweets' });
  },

};
//--------------------------------------------------------------------------------------------------------------
exports.login = {
  auth: false, // means password NOT required, authorisation not required on general pages only, ie. NOT protected routes
  handler: function (request, reply) {
    reply.view('login', { title: 'Login to Tweets' });
  },
};
//--------------------------------------------------------------------------------------------------------------
/**
 * authenticate using HAPI event handler
 * to consult the database when validating a user
 * No longer using cookies
 * Login page action
 */
exports.authenticate = {
  auth: false,

  validate: {

    payload: { //defines a schema which defines rules that the fields must adhere to
      email: Joi.string().email().required(),
      password: Joi.string().required(),
      // OK FOR ADMINISTRATOR?
    },

    options: {
      abortEarly: false,
    },

    // the handler to invoke if one or more of the fields fails the validation:
    failAction: function (request, reply, source, error) {
      reply.view('Login', {
        title: 'Login error',
        errors: error.data.details,
      }).code(400);
    },
  },

  handler: function (request, reply) {
    const user = request.payload;  // Create a Document..???

    User.findOne({ email: user.email }).then(foundUser => {
      if (foundUser && foundUser.password === user.password) {
        // Set the cookie if correct user credentials presented:
        // If cookie set, it can be read back in any handler
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: user.email,
        });
        reply.redirect('/home');
      } else {
        reply.redirect('/signup');
      }
    }).catch(err => {
      reply.redirect('/');
    });
  },
};
//--------------------------------------------------------------------------------------------------------------
/**
 * Register HAPI Event Handler
 * Signup page action
 */
exports.register = {
  auth: false,

  validate: { //joi for validation

    payload: { //defines a schema which defines rules that the fields must adhere to
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },
    options: {
      abortEarly: false,
    },

    // the handler to invoke if one or more of the fields fails the validation:
    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },
  },

  handler: function (request, reply) {
    // Request PayedLoad sends data as json object....?
    const user = new User(request.payload); // Create a Document..???
    user.save().then(newUser => { //Save the Document (Promises): newUser is the saved object
      reply.redirect('/login'); // success : user saved successfully
    }).catch(err => { //an Error has occurred
      reply.redirect('/');
    });
  },
};
//--------------------------------------------------------------------------------------------------------------
/**
 * Settings page view
 */
exports.viewSettings = {

  handler: function (request, reply) {
  var userEmail = request.auth.credentials.loggedInUser;

    //to read from the database to get the user details, and then
    // render these to the view (sending the user to the start page if there is an error):
    User.findOne({ email: userEmail }).then(foundUser => { // One email attribute we as searching on
      //console.log('View settings for: ' + user.email);
      //DB Query success, check foundUser to see if match
      reply.view('settings', {
        title: 'Edit Account Settings',
        user: foundUser
      });
    }).catch(err => { //error accessing DB
      reply.redirect('/');
    });
},

};
//--------------------------------------------------------------------------------------------------------------
/**
 * updateSettings HAPI event handler
 * Settings page action
 */
exports.updateSettings = {

  validate: {

    payload: { //defines a schema which defines rules that the fields must adhere to
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    options: {
      abortEarly: false,
    },

    // the handler to invoke if one or more of the fields fails the validation:
    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },
  },

  handler: function (request, reply) {

    const editedUser = request.payload;
    const loggedInUserEmail = request.auth.credentials.loggedInUser;

    // To read a users details from the database, and
    // then update with new values entered by the user
    //In mongoose 4, a Query has a .then() function, and thus can be used as a promise.
    User.findOne({ email: loggedInUserEmail }).then(user => { //DB Query
      console.log('Updating settings settings for: ' + user.email);
      //Query succeeded, replace the fields:
      user.firstName = editedUser.firstName;
      user.lastName = editedUser.lastName;
      user.email = editedUser.email;
      user.password = editedUser.password;

      // Mongoose async operations, like .save() and queries return Promises
      //Save the new version:
      return user.save(); // return a promise from the save() function -
      // and then re-render the updated user details to the settings view:
    }).then(user => { //New version saved
      //reply.view('userPublicTimeline', { title: 'Updated Account Settings', user: user });
      reply.view('settings', {
        title: 'Updated Account Settings',
        user: user
      });
      console.log('Updated user settings. name: ' + user.firstName + ' ' + user.lastName + ' , email: ' + user.email + ' , password: '  + user.password  );

    }).catch(err => {
      reply.redirect('/');
    });
  },
};
//--------------------------------------------------------------------------------------------------------------
//  clear the session
exports.logout = {
  auth: false, //Any attempt to access protected routes rejected
  handler: function (request, reply) {
    request.cookieAuth.clear(); //Cookie deleted
    reply.redirect('/');
  },
};
//--------------------------------------------------------------------------------------------------------------
// ADMINISTRATOR:

exports.admin = {
  auth: false, // means password NOT required, authorisation not required on general pages only, ie. NOT protected routes
  handler: function (request, reply) {
    reply.view('adminLogin', { title: 'Administrator Login' });
  },
};
//------------------------------------------------------------- ADMIN contd....
exports.adminLogin = {
  auth: false, // means password NOT required,

  validate: {

    payload: { //defines a schema which defines rules that the fields must adhere to
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    options: {
      abortEarly: false,
    },

    // the handler to invoke if one or more of the fields fails the validation:
    failAction: function (request, reply, source, error) {
      reply.view('Login', {
        title: 'Login error',
        errors: error.data.details,
      }).code(400);
    },
  },

  handler: function (request, reply) {
    const user = request.payload;  // Create a Document..???

    Administrator.findOne({ email: user.email }).then(foundAdmin => {
      if (foundAdmin && foundAdmin.password === user.password) {
        // Set the cookie if correct user credentials presented:
        // If cookie set, it can be read back in any handler
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: user.email,
        });
        console.log('Admin: ' + user.email + ' logged in');
        reply.redirect('/adminHome');
      } else {
        console.log('Admin: ' + user.email + ' failed to logged in');
        reply.redirect('/admin');
      }
    }).catch(err => {
      console.log('Failed to login');
      reply.redirect('/admin');
    });
  },
};

