/**
 *  js files: the database queries are using www.mongoosejs.com
 * Controller to support making and listing tweets
 * Normalised tweets
 */

'use strict';

//the blog and timeline routes use this model
// when we create a tweet, we need to gain access to the object reference of the user and
// use this reference to initialise the tweet object
const Tweet = require('../models/tweet'); // import the Model
const User = require('../models/user'); // import the User model
const Administrator = require('../models/administrator'); // import the Administrator model...needed ..???

// Handlers:
// switch to anonymous function .. to allow us access global variables later
// home an object literal; exporting an object
// one name:value pair 'handler' (an attribute)
// expects handler to be function - defined using arrow function..?
// a callback
// where we implement controllers
// could have security, validation here also
exports.home = {

  // always 2 parameters .. Node request
  handler: function (request, reply) {
    // sending back the view:
    // .vew a method on object reply
    // equivalent to play 'render'
    // reply could have different methods depending on what returning
    reply.view('home', {title: 'Post your Tweet!'});
  },

};
//---------------------------------------------------------------------------------------------------------------
/**
 *  blog handler establishes the link to the tweet
 *  tweet schema model stores information about the user
 *  Record tweet + user when creating tweet
 *  when we create a tweet, we will need to gain access to the object reference of the user and use
 *  this reference to initialise the tweet object
 *
 * Creating a tweet:  locate the current user in the database,
 * also locate the preferred timeline (public user timeline or global timeline) to contribute to.
 * Only when these two queries are performed can we create and insert a new tweet object
 */
exports.blog = {

  handler: function (request, reply) {
    //reimplement the tweet handler to establish the link to the tweet

    //Identify logged-in user:
    var userEmail = request.auth.credentials.loggedInUser;
    // Find the user from their email
    User.findOne({email: userEmail}).then(user => {

      //Create new tweet object
      // payload body of a HTTP Request - is the data normally send by a POST Request.
      let data = request.payload; // refers to incoming data from the html (POST attribute)
      const tweet = new Tweet(data);
      tweet.user = user._id; //Link to logged_in_user id; used in Story 5 to delete tweets
      tweet.date = new Date(); // save date Story 4
     // return tweet.save(); //Save the tweet object

      // To attach image
      //If there is no picture, length is undefined and the check fails and is skipped.
      // If there is a picture, length is the number of bytes, the check passes and the picture is saved
      if (data.picture.length) {
        tweet.picture.data = data.picture;
        tweet.picture.contentType = 'jpg'; //, 'png', 'gif'; // seems to work for all file types..???

      }
      //Save the tweet object
      return tweet.save();

    }).then(newTweet => {
      reply.redirect('/userTimeline'); // send to the  public user timeline

    }).catch(err => {


      reply.redirect('/');
    });
  },

};
//---------------------------------------------------------------------------------------------------------------
/**
 * Formats dates on user/global timeline
 * Uses HandleBars.registerHelper to create helper method using momentjs to format dates.
 */
var Handlebars = require('handlebars');
const moment = require('moment');

Handlebars.registerHelper('formatDate', function (timestamp) {
  // Formatting the date using momentjs
  return moment(timestamp).format('DD MMM YYYY HH:mm:SS');
});
//---------------------------------------------------------------------------------------------------------------
/**
 * Send all tweets to the view
 *  ensure that the user object will be retrieve on the single query - and
 *  thus the user object should successfully resolve in the form
 */
exports.userPublicTimeline = {

  handler: function (request, reply) {

    // Populated paths are no longer set to their original _id ,
    // their value is replaced with the mongoose document returned from the database (containing the tweets ???)
    // by performing a separate query before returning the results
    //reimplement the tweet handler to establish the link to the tweet

    //Identify logged-in user's email
    var userEmail = request.auth.credentials.loggedInUser;
    console.log('User email: ' + userEmail);

    // Find the user from their email
    User.findOne({email: userEmail}).then(foundUser => {
      console.log('Found user: ' + foundUser.firstName);

      // to support tweet statistics for Tweet model
      var stats = new Object();

      // total no tweets for the loggedInUser:
      Tweet.count({user: foundUser }, function (err, tweets) {
        stats.count = tweets;
      });

      // Mongoose query by time/dates range using momentjs
      Tweet.count({ date: { $gt: moment().subtract(1, 'hour') } }, function (err, tweets) {
        stats.countHour = tweets;
      });

      // uses the query result as the parameter for the second query promise...????
      // Find the tweets for this foundUser
      // Populate the user property
      // Uses momentjs (.sort) to format dates and sort in ascending order
      Tweet.find({user: foundUser}).populate('user').sort({ date: 'asc' }).then(userTweets => { // the user object will be retrieved on the single query
        console.log('Found user: ' + userTweets.length + ' tweets from ' + foundUser.firstName + ' ' + foundUser.lastName);

        // Display the userPublicTimeline and pass in the user's tweets
        reply.view('userPublicTimeline', {
          title: 'Tweeter Timeline',
          stats: stats,
          tweets: userTweets,
          canDelete: true, // user can delete their tweets on User Public Timeline; called up in html
        });
      }).catch(err => {
        reply.redirect('/');
      });
    });
    },
  };
//---------------------------------------------------------------------------------------------------------------
/**
 * Send all tweets to the view
 *  ensure that the user object will be retrieve on the single query - and
 *  thus the user object should successfully resolve in the form
 */
exports.globalTimeline = {

  handler: function (request, reply) {

    //Default behaviour for find to return only return ids in place of tweet
    //Populated paths are no longer set to their original _id , their value is replaced with the mongoose document returned
    // from the database by performing a separate query before returning the results

    // Find all the tweets
    // Populate the user property with the associated tweets
    // Uses momentjs (.sort) to format dates and sort in ascending order
    Tweet.find({}).populate('user').sort({ date: 'asc' }).then(allTweets => { // the tweet object will be retrieved on the single query

      // Display the globalTimeline and pass in all tweets of all users
      reply.view('globalTimeline', {
        title: 'Global Timeline',
        tweets: allTweets,
        canDelete: false, // user cannot delete tweets on global timeline; called up in html
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};
//---------------------------------------------------------------------------------------------------------------
/**
 * V_05: Allow user to delete individual user tweets from User Public Timeline
 */
exports.deleteTweet = {

  handler: function(request, reply){

  var tweetId = request.params.tweetId; // syntax to retrieve existing object

    // Remove one tweet
    Tweet.remove({_id: tweetId }, function (err) { //callback
      if (err) return 'err';
      console.log(err);
    });
    reply.redirect('/userTimeline')
  },
};
//---------------------------------------------------------------------------------------------------------------
/**
 * V_06: Allow User to delete all their tweets from User Public Timeline
 */
exports.deleteAll = {

  handler: function(request, reply){

    //Identify logged-in user's email
    var userEmail = request.auth.credentials.loggedInUser;
    console.log('Deleting all tweets for: ' + userEmail);

    // Find the user from their email
    User.findOne({email: userEmail}).then(foundUser => {
        console.log('Found user: ' + foundUser.firstName + ' ' + foundUser.lastName);

    // Tweet list already populated at userPublicTimeline handler
    // Delete all tweets for that logged-in User
    Tweet.remove({ user: foundUser }, function (err) {
      if (err) return 'err'; // asynchronous method takes a completion callback as last argument??
      console.log(err);
    });
  });
    reply.redirect('/userTimeline')
  },
};
//-------------------------------------------------------------------------------------------------------------
/**
 * To find other user from semantic cards link and populate timeline of another selected user on otherUserTimeline view
 */
exports.findOtherUser = {

  handler: function(request, reply) {

    let userId = request.params.userId;// syntax to retrieve existing object, isolating the ID only
    console.log('Other User Id: ' + userId);

    // Find the user from their id; callback arrow notation
    User.findOne({_id: userId}).then(foundUser => { // the user object will be retrieved on the single query
      console.log('Found other user: ' + foundUser.email);

      //Tweet.find({user: userId}).then(userTweets => {
      //Tweet.find({user: foundUser}).then(userTweets => { // the user object will be retrieved on the single query
      //console.log('User to edit: ' + user.email);

      // Uses momentjs (.sort) to format dates and sort in ascending order
      Tweet.find({user: foundUser}).populate('user').sort({ date: 'asc' }).then(userTweets => { // the user object will be retrieved on the single query
        console.log('Found Other User: ' + userTweets.length + ' tweets from ' + foundUser.firstName + ' ' + foundUser.lastName);

        // Display the User Maintenance View and pass in the user object
        reply.view('otherUserTimeline', {
          title: 'otherUserTimeline',
          tweets: userTweets,
          user: foundUser, // already populated above
          canDelete: true, // Administrator can delete the User
        });
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};
//-------------------------------------------------------------------------------------------------------------
/**
 * To view the timeline of another from the user login, and hyperlink from Global Timeline user name
 */
exports.userFindOtherUser = {

  handler: function(request, reply) {

    let userId = request.params.userId;// syntax to retrieve existing object, isolating the ID only
    console.log('Other User Id: ' + userId);

    // Find the user from their id; callback arrow notation
    User.findOne({_id: userId}).then(foundUser => { // the user object will be retrieved on the single query
      console.log('Found other user: ' + foundUser.email);

      //Tweet.find({user: userId}).then(userTweets => {
      //Tweet.find({user: foundUser}).then(userTweets => { // the user object will be retrieved on the single query
      //console.log('User to edit: ' + user.email);

      // Uses momentjs (.sort) to format dates and sort in ascending order
      Tweet.find({user: foundUser}).populate('user').sort({ date: 'asc' }).then(userTweets => { // the user object will be retrieved on the single query
        console.log('Found Other User: ' + userTweets.length + ' tweets from ' + foundUser.firstName + ' ' + foundUser.lastName);

        // Display the User Maintenance View and pass in the user object
        reply.view('userOtherUserTimeline', {
          title: 'userOtherUserTimeline',
          tweets: userTweets,
          user: foundUser, // already populated above
          canDelete: false, // Administrator can delete the User
        });
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};
//---------------------------------------------------------------------------------------------------------------
/**
 * To allow images within tweets
 */
exports.getPicture = {

  handler: function (request, reply) {
    let tweetId = request.params.tweetId;

    Tweet.findOne({ _id: tweetId }).exec((err, tweet) => {
      if (tweet.picture) {
        console.log(tweet.picture.data);
        reply(tweet.picture.data).type('image');
      }
    });
  },
};
/**
 * To check if a tweet contains a picture
 * http://stackoverflow.com/questions/17095813/handlebars-if-and-numeric-zeroes
 */
Handlebars.registerHelper('exists', function (variable, options) { //callback
  console.log('Options ' + options);
  if (typeof variable !== 'undefined') {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});