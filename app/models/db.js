'use strict';
/**
 * Promises implements a simpler and more robust pattern for asynchronous programming
 * Gradually replacing Callbacks in many libraries and applications
 * A promise has 3 states: Pending , Fulfilled, Rejected
 * Once callbacks become nested, promises a simpler approach e.g: interacting with a database
 * to lookup multiple objects, modify them and then save updates if no errors have occurred
 *
 * db.js: uses moogoose-seeder
 */

// establishes a connection to the database
const mongoose = require('mongoose'); //import mongoose
// using  promise syntax for database query and update -
// //however the default promise support we are using is not intended for long term use:
mongoose.Promise = global.Promise; //turn on native ES6 promises library instead

// declare the connection string
//let dbURI = 'mongodb://localhost/microblog_webent'; //to connect to the mongodB database
let dbURI = 'mongodb://microbloguser:microbloguser@ds143707.mlab.com:43707/microblog';
if (process.env.NODE_ENV === 'production') {
  dbURI = process.env.MONGOLAB_URI;
}

mongoose.connect(dbURI); // connect to the database

/**
 * Log success/fail/disconnect
 */
mongoose.connection.on('connected', function () {
  console.log('Mongoose connected to ' + dbURI);
  if (process.env.NODE_ENV != 'production') {

    // When we define a new model, we must load it before attempting to seed the database
    var seeder = require('mongoose-seeder');
    const data = require('./inidata.json');
    const Tweet = require('./tweet');
    const User = require('./user');
    const Administrator = require('./administrator');

    //STORY 3:
    // dropCollections: true to initially load the test/inidata data
    // dropCollections: false to persist existing(preloaded inidata) data once loaded
/*    seeder.seed(data, { dropDatabase: false, dropCollections: false}).then(dbData => {
      console.log('preloading Test Data');
      console.log(dbData);
    }).catch(err => {
      console.log(error);
    });*/
  }
});

mongoose.connection.on('error', function (err) {
  console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function () {
  console.log('Mongoose disconnected');
});