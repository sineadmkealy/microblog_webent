/**
 * A tweet schema to represent individual tweets
 * Normalised Users and Tweets
 * Normalized data models describe relationships using references between documents.
 */

const mongoose = require('mongoose');

// equivalent to foreign key
const tweetSchema = mongoose.Schema({
  date: Date,
  content: String,
  picture: { data: Buffer, contentType: String },
 // email: String,
  //Reference encapsulated as object reference to tweet object:
  user: { //use an object reference directly to the User object (provides further information on the user)
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
});
// ADMINISTRATOR????

const Tweet = mongoose.model('Tweet', tweetSchema);
module.exports = Tweet;

