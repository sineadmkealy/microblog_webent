/**
 * Routes that serves the controllers
 *  routes.js uses hapi : wwww.hapijs.com
 */

// object Tweets ..get from route
const Accounts = require('./app/controllers/accounts');
const Tweets = require('./app/controllers/tweets');
const Admin = require('./app/controllers/admin');
const Assets = require('./app/controllers/assets');

// module system
module.exports = [

    // an actual js programme:
    // an object literal that is an array of routes
    // each element another object of name:value pairs
    // method, path, config
  { method: 'GET', path: '/', config: Accounts.main },
  { method: 'GET', path: '/signup', config: Accounts.signup },
  { method: 'GET', path: '/login', config: Accounts.login },
  { method: 'POST', path: '/login', config: Accounts.authenticate },
  { method: 'POST', path: '/register', config: Accounts.register },
  { method: 'GET', path: '/logout', config: Accounts.logout },
  { method: 'GET', path: '/settings', config: Accounts.viewSettings }, // to support rendering the form
  { method: 'POST', path: '/settings', config: Accounts.updateSettings }, // to support submitting the form

  { method: 'GET', path: '/admin', config: Accounts.admin },
  { method: 'POST', path: '/adminLogin', config: Accounts.adminLogin },//a POST route to accept Admin login details

  { method: 'GET', path: '/home', config: Tweets.home }, //GET routes to display tweets
  { method: 'GET', path: '/userTimeline', config: Tweets.userPublicTimeline },
  { method: 'GET', path: '/globalTimeline', config: Tweets.globalTimeline },
  { method: 'GET', path: '/findOtherUser/{userId}', config: Tweets.findOtherUser  },
  { method: 'GET', path: '/userFindOtherUser/{userId}', config: Tweets.userFindOtherUser  },

  { method: 'POST', path: '/blog', config: Tweets.blog },  //a POST route to accept/save tweets
  { method: 'GET', path: '/deleteTweet/{tweetId}', config: Tweets.deleteTweet },
  { method: 'POST', path: '/deleteAll', config: Tweets.deleteAll },
  { method: 'GET', path: '/getPicture/{tweetId}', config: Tweets.getPicture },

  { method: 'GET', path: '/adminHome', config: Admin.home },
  { method: 'GET', path: '/adminUsers', config: Admin.users },
  { method: 'GET', path: '/userAdd', config: Admin.userAdd },
  { method: 'GET', path: '/userEdit/{userId}', config: Admin.userEdit },
  { method: 'POST', path: '/userSave/{userId}', config: Admin.userSave },
  { method: 'GET', path: '/userDelete/{userId}', config: Admin.userDelete },
  { method: 'POST', path: '/adminDeleteAllUsers', config: Admin.adminDeleteAllUsers },

  { method: 'GET', path: '/adminDeleteTweet/{tweetId}', config: Admin.adminDeleteTweet },
  { method: 'POST', path: '/adminDeleteAll', config: Admin.adminDeleteAll },
  { method: 'POST', path: '/adminDeleteAllOtherUserTweets/{userId}', config: Admin.adminDeleteAllOtherUserTweets },



  // HAPI syntax, plugin for static assets
  {

    method: 'GET',
    path: '/{param*}',
    config: { auth: false },
    handler: Assets.servePublicDirectory,
  },



];