'use strict';

const Hapi = require('hapi'); // npm install HAPI, here grabbing the HAPI object

var server = new Hapi.Server(); // Server running all time, handling requests
// method on server, takes object {} name:value pair:
server.connection({ port: process.env.PORT || 4000 });  // named PORT value is 4000 or other variable

// With introduction of Mondgo db, delete the exsiting server objects below:
// Trigger connection by importing db.js from index
require('./app/models/db'); //import of the mongo db module

// 1
// register/tell what plugins are using
// arrow function a callback checking for error
server.register([require('inert'), require('vision'), require('hapi-auth-cookie')], err => {

  if (err) {
    throw err;
  }

  // handlebars engine
  //Handlebars provides the power to  build semantic templates effectively
  server.views({
    engines: {
      hbs: require('handlebars'),
    },
    relativeTo: __dirname,
    path: './app/views',
    layoutPath: './app/views/layout',
    partialsPath: './app/views/partials',
    layout: true,
    isCached: false,
  });

  // initialize the plugin
  // paramaters set a secure password for the cookie itself,
  // a name for the cookie and a time to live (1 day).
  // Additionally, it is set to work over non-secure connections.
  server.auth.strategy('standard', 'cookie', {
    password: 'secretpasswordnotrevealedtoanyone',
    cookie: 'donation-cookie',
    isSecure: false,
    // If route protected, and cookie deleted/ timeout:
    ttl: 24 * 60 * 60 * 1000,
    redirectTo: '/login', // Redirect any protected routes to Login Page
  });

  /**
   * By default hapi-auth-cookie will only allow the cookie to be transferred over a secure TLS/SSLconnection
   * inconvenient during development so set the isSecure option to false.
   * ‘standard’ as the default strategy for all routes : routes protected with the standard security strategy
   *
   * All routes ‘guarded’ by default, cookie-based authentication mechanism
   * Any attempt to visit a route will be rejected unless valid cookie detected
   * Some routes need to be available (to signup or login) - must disable auth mechanism in accounts
   */
  server.auth.default({
    strategy: 'standard',
  });

  // 2
// calling method 'route' on HAPI server: here is routes table
// require will return/export it from routes
  server.route(require('./routes'));

// 3
// starting server, parameter another callback
  server.start(err => {
    if (err) {
      throw err;
    }
    // starts the service, get callback telling us service started + log message
    console.log('Server listening at:', server.info.uri);
  });

  // from now on all requests coming here and sending out files? from here
  // handler called every time a request called
  // exposed here, framework not monolithic, all separate pieces
});